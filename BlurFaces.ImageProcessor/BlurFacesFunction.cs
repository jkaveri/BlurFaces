using BlurFaces.Shared.Configs;
using BlurFaces.Shared.Helpers;
using BlurFaces.Shared.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Convolution;
using SixLabors.Primitives;
using System;
using System.IO;
using System.Threading.Tasks;

namespace BlurFaces.Shared
{
    public static class BlurFacesFunction
    {
        [FunctionName("BlurFacesFunction")]
        public static async Task Run(
            [ServiceBusTrigger(
               queueName: ServiceBusConfig.FACES_DETECTED_QUEUE_NAME,
               access: AccessRights.Manage,
               Connection = SettingKeys.SERVICE_BUS_CONNECTION
            )]
            string mySbMsg,
            TraceWriter log)
        {
            try
            {
                if (string.IsNullOrEmpty(mySbMsg))
                {
                    return;
                }

                // images container storage.
                var storageHelper = new AzureStorageHelper(AzureStorageConfig.CreateFromEnvironments());

                var model = JsonConvert.DeserializeObject<FacesDetectedModel>(mySbMsg);

                if (string.IsNullOrEmpty(model.FileName))
                {
                    return;
                }

                var tempFilePath = Guid.NewGuid().ToString();
                var destTempFilePath = Guid.NewGuid().ToString();
                var blobBlock = await storageHelper.GetImageBlobAsync(AzureStorageConfig.ImagesContainer, model.FileName);

                await blobBlock.DownloadToFileAsync(tempFilePath, FileMode.CreateNew);

                Image<Rgba32> image = Image.Load<Rgba32>(tempFilePath);

                using (var outputStream = File.OpenWrite(destTempFilePath))
                {
                    image.Mutate(x =>
                    {
                        foreach (var description in model.FaceDescriptions)
                        {
                            var rect = description.FaceRectangle;
                            x.BoxBlur(30, new Rectangle(rect.Left, rect.Top, rect.Width, rect.Height));
                        }
                    });

                    image.SaveAsPng(outputStream);
                }

                using (var stream = File.OpenRead(destTempFilePath))
                {
                    var newFileName = $"{Path.GetFileName(model.FileName)}_blurred.{Path.GetExtension(model.FileName)}";
                    await storageHelper.UploadFileToStorage(AzureStorageConfig.BlurredImagesContainer, destTempFilePath, newFileName);
                }

                log.Info($"C# ServiceBus topic trigger function processed message: {mySbMsg}");
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
}
