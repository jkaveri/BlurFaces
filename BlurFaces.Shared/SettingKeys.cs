﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurFaces.Shared
{
    public class SettingKeys
    {
        public const string STORAGE_CONNECTION = "AzureWebJobsStorage";
        public const string SERVICE_BUS_CONNECTION = "ServiceBusConnection";
        public const string AZURE_COGNITIVE_SUBSCRIPTION_KEY = "AzureCognitiveSubscriptionKey";
        public const string AZURE_COGNITIVE_API_REGION = "AzureCognitiveApiRegion";
    }
}
