﻿using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using System;

namespace BlurFaces.Shared.Configs
{
    public class AzureCognitiveConfig
    {
        public string SubscriptionKey { get; set; }
        public AzureRegions AzureRegion { get; set; }

        public static AzureCognitiveConfig CreateFromEnvironments()
        {
            var regionStr = Environment.GetEnvironmentVariable(SettingKeys.AZURE_COGNITIVE_API_REGION);
            var region = (AzureRegions)Enum.Parse(typeof(AzureRegions), regionStr, true);
            return new AzureCognitiveConfig
            {
                SubscriptionKey = Environment.GetEnvironmentVariable(SettingKeys.AZURE_COGNITIVE_SUBSCRIPTION_KEY),
                AzureRegion = region
            };
        }
    }
}