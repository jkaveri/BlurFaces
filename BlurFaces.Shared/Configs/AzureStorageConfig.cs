﻿using System;

namespace BlurFaces.Shared.Configs
{
    public class AzureStorageConfig
    {
        public const string ImagesContainer = "images";
        public const string BlurredImagesContainer = "blurredimages";

        public string ConnectionString { get; set; }

        public static AzureStorageConfig CreateFromEnvironments()
        {
            return new AzureStorageConfig
            {
                ConnectionString = Environment.GetEnvironmentVariable(SettingKeys.STORAGE_CONNECTION)
            };
        }
    }
}
