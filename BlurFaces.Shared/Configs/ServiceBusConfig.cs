﻿using System;

namespace BlurFaces.Shared.Configs
{
    public class ServiceBusConfig
    {
        public const string FACES_DETECTED_QUEUE_NAME = "faces_detected_queue";

        public string ConnectionString { get; set; }

        public static ServiceBusConfig CreateFromEnvironments()
        {
            var connection = Environment.GetEnvironmentVariable(SettingKeys.SERVICE_BUS_CONNECTION);
            return new ServiceBusConfig
            {
                ConnectionString = connection
            };
        }
    }
}