﻿using BlurFaces.Shared.Configs;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using SixLabors.ImageSharp;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace BlurFaces.Shared.Helpers
{
    public class AzureStorageHelper
    {
        private readonly AzureStorageConfig _storageConfig;

        public AzureStorageHelper(AzureStorageConfig config)
        {
            _storageConfig = config;

            if (string.IsNullOrEmpty(_storageConfig.ConnectionString))
            {
                throw new InvalidOperationException("Missing storage config");
            }
        }

        public async Task<Uri> UploadFileToStorage(string conatiner, string filePath, string fileName, CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var stream = File.OpenRead(filePath))
            {
                return await UploadFileToStorage(conatiner, stream, fileName, cancellationToken);
            }
        }

        public async Task<Uri> UploadFileToStorage(string container, Stream fileStream, string fileName, CancellationToken cancellationToken = default(CancellationToken))
        {
            var imageFormat = Image.DetectFormat(fileStream);

            var imageContainer = await CreateImageBlobContainerAsync(container);
            CloudBlockBlob blockBlob = imageContainer.GetBlockBlobReference(fileName);

            await blockBlob.UploadFromStreamAsync(fileStream).ConfigureAwait(false);

            return blockBlob.Uri;
        }

        public async Task<CloudBlockBlob> GetImageBlobAsync(string conatiner, string fileName)
        {
            var container = await CreateImageBlobContainerAsync(conatiner);

            var blobBlock = container.GetBlockBlobReference(fileName);

            return blobBlock;
        }

        public async Task<CloudBlobContainer> CreateImageBlobContainerAsync(string containerName)
        {
            var storageAccount = CloudStorageAccount.Parse(_storageConfig.ConnectionString);

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            var container = blobClient.GetContainerReference(containerName);

            await container.CreateIfNotExistsAsync();

            return container;
        }
    }
}
