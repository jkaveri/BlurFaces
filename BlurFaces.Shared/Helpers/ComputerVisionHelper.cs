﻿using BlurFaces.Shared.Configs;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Microsoft.Rest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurFaces.Shared.Helpers
{
    public class ComputerVisionHelper
    {
        private readonly AzureCognitiveConfig _config;

        public ComputerVisionHelper(AzureCognitiveConfig config)
        {
            _config = config;
        }

        public async Task<IList<FaceDescription>> DetectFaces(Stream stream)
        {
            var credentials = new ApiKeyServiceClientCredentials(_config.SubscriptionKey);

            var api = new ComputerVisionAPI(credentials)
            {
                AzureRegion = _config.AzureRegion
            };
            var result = await api.AnalyzeImageInStreamAsync(stream, new[] { VisualFeatureTypes.Faces });
            return result.Faces;
        }
    }
}
