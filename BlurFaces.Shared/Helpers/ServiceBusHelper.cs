﻿using BlurFaces.Shared.Configs;
using BlurFaces.Shared.Models;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BlurFaces.Shared.Helpers
{
    public class ServiceBusHelper
    {
        private readonly ServiceBusConfig _config;

        public ServiceBusHelper(ServiceBusConfig config)
        {
            if (string.IsNullOrEmpty(config.ConnectionString))
            {
                throw new InvalidCastException("Service connection cannot be null or empty");
            }
            _config = config;
        }

        public async Task SendFacesDetectedMessage(IList<FaceDescription> faces, string fileName)
        {
            // Create a sender over the previously configured duplicate-detection
            // enabled queue.
            var queueClient = new QueueClient(_config.ConnectionString, ServiceBusConfig.FACES_DETECTED_QUEUE_NAME);
            // Create the message-id
            string messageId = Guid.NewGuid().ToString();
            var jsonBody = JsonConvert.SerializeObject(new FacesDetectedModel
            {
                FaceDescriptions = faces,
                FileName = fileName
            });
            var body = UTF8Encoding.Default.GetBytes(jsonBody);
            var message = new Message()
            {
                MessageId = messageId,
                Body = body,
                TimeToLive = TimeSpan.FromMinutes(30)
            };

            await queueClient.SendAsync(message);
        }
    }
}
