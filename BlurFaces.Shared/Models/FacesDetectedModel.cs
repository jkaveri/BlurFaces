﻿using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurFaces.Shared.Models
{
    public class FacesDetectedModel
    {
        public string FileName { get; set; }
        public IList<FaceDescription> FaceDescriptions { get; set; }
    }
}
