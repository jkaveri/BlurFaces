using BlurFaces.Shared;
using BlurFaces.Shared.Configs;
using BlurFaces.Shared.Helpers;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using System.IO;
using System.Threading.Tasks;

namespace BlurFaces.ComputerVision
{
    public static class DetectFacesFunction
    {
        [FunctionName("DetectFacesFunction")]
        public static async Task Run(
            [BlobTrigger(
                "images/{fileName}.{fileExtension}",
                Connection = SettingKeys.STORAGE_CONNECTION
            )]
            Stream imageBlob,
            string fileName,
            string fileExtension,
            TraceWriter log)
        {
            var fullFileName = $"{fileName}.{fileExtension}";

            var visionHelper = new ComputerVisionHelper(AzureCognitiveConfig.CreateFromEnvironments());
            var serviceBusHelper = new ServiceBusHelper(ServiceBusConfig.CreateFromEnvironments());
            var faces = await visionHelper.DetectFaces(imageBlob);

            if (faces.Count > 0)
            {
                await serviceBusHelper.SendFacesDetectedMessage(faces, fullFileName);
            }

            log.Info($"C# Blob trigger function Processed blob\n Name:{fileName}.${fileExtension} \n Size: {imageBlob.Length} Bytes");
        }
    }
}