using BlurFaces.Shared.Configs;
using BlurFaces.Shared.Helpers;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace BlurFaces.Shared
{
    public static class UploadImageFunction
    {
        [FunctionName("UploadImageFunction")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "images/upload")]
            HttpRequestMessage req,
            TraceWriter log,
            CancellationToken cancellationToken)
        {
            try
            {
                if (!Directory.Exists("./Upload_Temp"))
                {
                    Directory.CreateDirectory("./Upload_Temp");
                }
                var provider = new MultipartFormDataStreamProvider("./Upload_Temp");
                await req.Content.ReadAsMultipartAsync(provider);

                var storageHelper = new AzureStorageHelper(AzureStorageConfig.CreateFromEnvironments());
                var containter = AzureStorageConfig.ImagesContainer;
                Uri uri = null;
                foreach (var file in provider.FileData)
                {
                    var fileName = file.Headers.ContentDisposition.FileName.Trim('"');
                    uri = await storageHelper.UploadFileToStorage(containter, file.LocalFileName, fileName, cancellationToken);
                    log.Info($"Uploaded file {fileName}");
                    break;
                }

                return req.CreateResponse(HttpStatusCode.Accepted, uri);
            }
            catch (Exception ex)
            {
                return req.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}